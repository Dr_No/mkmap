#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "mkmap.h"
#include "mkmap_window.h"
#include <QApplication>

#ifdef WIN32
#include <windows.h>
#endif

static void show_usage(std::string name)
{
    std::string out_text;

    out_text += "Usage: " + name + " <option(s)>\n";
    out_text += "Options:\n";
    out_text += "\t-h,--help\t\tShow this help message\n";
    out_text += "\t--version\t\tShow the version\n";

    std::cerr << out_text << std::endl;
}

static void show_version(std::string name)
{
    std::string out_text;

    out_text += name + ":\n";
    out_text += "A LED layout mapper for Coolermaster Masterkey keyboards\n\n";
    out_text += "Version:\t\t" + std::to_string(VERSION_MAJOR) + "." + std::to_string(VERSION_MINOR) + "\n";
    out_text += "Git Branch:\t\t";
    out_text += GIT_BRANCH;
    out_text += "\nGit Commit:\t\t";
    out_text += GIT_COMMIT_HASH;

    std::cout << out_text << std::endl << std::endl;
}

int process_args(int argc, char* argv[])
{
    int arg_err = 0;

    if (argc < 1) {
        show_usage(argv[0]);
        arg_err = 1;
    }

    for (int i = 1; i < argc; ++i) {
        std::string arg = argv[i];

        if (arg == "--version") {
            show_version(argv[0]);
        } else {
            //Catch everything else including ((arg == "-h") || (arg == "--help"))
            show_usage(argv[0]);
        }
    }

    return arg_err;
}

int main(int argc, char* argv[])
{
    int main_err = process_args(argc, argv);
    QApplication mkmap_app(argc, argv);

    mkmap_window    mkmap;
    mkmap.show();

    return mkmap_app.exec();
}
