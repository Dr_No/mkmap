#ifndef MKMAP_WINDOW_H
#define MKMAP_WINDOW_H

#include <QMainWindow>
#include <QComboBox>
#include <QErrorMessage>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QInputDialog>
#include <QKeyEvent>
#include <QMessageBox>
#include <QPushButton>
#include <QSpacerItem>
#include <QString>
#include <QStringList>
#include <QVariant>
#include <QVBoxLayout>

#include <filesystem>
#include <fstream>
#include <iostream>
#include <vector>

#ifdef _WIN32
    #include "output_maps_windows.h"
#else
    #include "output_maps_linux.h"
#endif
#include "hidapi/hidapi.h"
#include "libcmmk/libcmmk.h"
#include "key_capture_dialog.h"

#define COOLERMASTER_VID        0x2516
#define COOLERMASTER_USAGE      01
#define COOLERMASTER_USAGE_PAGE 0xFF00

#define MKMAP_LED_ID_START      0
#define MKMAP_LED_ID_END        161
#define MKMAP_LED_ID_NA         NA     //Set the not available keycode beyond the range tested
#define MIN_WIDGET_SIZE         35
#define MAX_WIDGET_SIZE         100

/* Result codes */
enum mkmap_result {
    MKMAP_OK = 0,
    MKMAP_NODEVICE,         //No compatible device was found to map
    MKMAP_NOSPACEBAR,       //The spacebar ID was not found
};

QT_BEGIN_NAMESPACE
namespace Ui { class mkmap_window; }
QT_END_NAMESPACE

typedef std::vector<uint16_t> v_u16;

struct _kb_map
{
    v_u16   keys_known;
    v_u16   keys_unknown;
    v_u16   leds_unknown;
};

class mkmap_window : public QMainWindow
{
    Q_OBJECT

public:
    mkmap_window(QWidget *parent = nullptr);
    ~mkmap_window();

    uint16_t            getKeycode(uint16_t led_id);

    struct  cmmk        cmmk_dev;
    struct  rgb         black       = {  0,   0,   0};
    struct  rgb         red         = {255,   0,   0};
    struct  rgb         green       = {  0, 255,   0};
    struct  rgb         blue        = {  0,   0, 255};
    struct  rgb         yellow      = {255, 255,   0};
    struct  rgb         cyan        = {  0, 255, 255};
    struct  rgb         magenta     = {255,   0, 255};
    struct  rgb         white       = {255, 255, 255};

    void                keyPressEvent(QKeyEvent * ev);
    void                verifyKeys();
    void                paintKeyboard();
    void                checkKeyboardLayout();

private slots:
    std::string         libcmmk_format();
    std::string         openrgb_format();

    void                on_send_packets();
    void                on_action_New_Map_triggered();
    void                on_action_Save_Map_triggered();

private:
    Ui::mkmap_window   *ui;
    keyboard            kb;
    _kb_map             kb_map;
    bool                dirty = false;
    bool                cmmk_connected = false;

    std::string         hid_vendor;
    std::string         hid_product;
    std::string         hid_serial;

    bool                connected();
    bool                connected(int *cmmk_pid);
    bool                fallback_detection(QString *item);
    uint16_t            recurseKeys(uint16_t from, uint16_t to, QString keyname);
    void                setStatusOrRemap(QString led_string, QPushButton *button);

    template <class S, class V>
    void                findErase(S value, V *eraseFrom);
};
#endif // MKMAP_WINDOW_H
