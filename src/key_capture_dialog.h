#ifndef KEY_CAPTURE_DIALOG_H
#define KEY_CAPTURE_DIALOG_H

#include <QDialog>
#include <QKeyEvent>
#include <QPushButton>

#ifdef _WIN32
    #include "output_maps_windows.h"
#else
    #include "output_maps_linux.h"
#endif

namespace Ui {
class key_capture_dialog;
}

class key_capture_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit key_capture_dialog(QWidget *parent = nullptr, uint16_t *sc = nullptr, uint16_t led = 0);
    ~key_capture_dialog();

    void keyPressEvent(QKeyEvent * ev);

private slots:
    void on_cbx_special_keys_currentIndexChanged(int index);

private:
    uint16_t *scancode = nullptr;

    Ui::key_capture_dialog *ui;
};

#endif // KEY_CAPTURE_DIALOG_H
