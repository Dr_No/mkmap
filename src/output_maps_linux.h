#ifndef OUTPUT_MAPS_H
#define OUTPUT_MAPS_H

#define NA  0xFFFF

typedef std::map<uint16_t, std::string>         scancode_string;
typedef uint16_t                                kb_key;
typedef std::vector<kb_key>                     kb_row;
typedef std::vector<kb_row>                     keyboard;

/*-------------------------------------------------*\
| ANSI_Layout and ISO_Layout is a map of the        |
|   keycodes to an English string for output        |
\*-------------------------------------------------*/

static keyboard SIXTY_Layout =
{
    { true, true, true, true, true,false, true, true, true, true,false, true, true, true, true, true, true, true,false,false,false,false },
    { true, true, true, true, true, true, true, true, true, true, true, true, true,false, true, true, true, true,false,false,false,false },
    { true, true, true, true, true, true, true, true, true, true, true, true, true,false, true, true, true, true,false,false,false,false },
    { true, true, true, true, true, true, true, true, true, true, true, true,false,false, true,false,false,false,false,false,false,false },
    { true, true, true, true, true, true, true, true, true, true, true, true,false,false, true,false, true,false,false,false,false,false },
    { true, true, true,false,false,false, true,false,false,false, true, true, true,false, true, true, true, true,false,false,false,false }
};

static keyboard TKL_Layout =
{
    {   1,    1,    1,    1,    1,    0,    1,    1,    1,    1,    0,    1,    1,    1,    1,    1,    1,    1,    0,    0,    0,    0 },
    {   1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    0,    1,    1,    1,    1,    0,    0,    0,    0 },
    {   1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    0,    1,    1,    1,    1,    0,    0,    0,    0 },
    {   1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    0,    0,    1,    0,    0,    0,    0,    0,    0,    0 },
    {   1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    0,    0,    1,    0,    1,    0,    0,    0,    0,    0 },
    {   1,    1,    1,    0,    0,    0,    1,    0,    0,    0,    1,    1,    1,    0,    1,    1,    1,    1,    0,    0,    0,    0 }
};

static keyboard ANSI_Layout =
{
    {   9,   67,   68,   69,   70,   NA,   71,   72,   73,   74,   NA,   75,   76,   95,   96,  107,   78,  127,   NA,   NA,   NA,   NA },
    {  49,   10,   11,   12,   13,   14,   15,   16,   17,   18,   19,   20,   21,   NA,   22,  118,  110,  112,   77,  106,   63,   82 },
    {  23,   24,   25,   26,   27,   28,   29,   30,   31,   32,   33,   34,   35,   NA,   51,  119,  115,  117,   79,   80,   81,   86 },
    {  66,   38,   39,   40,   41,   42,   43,   44,   45,   46,   47,   48,   NA,   NA,   36,   NA,   NA,   NA,   83,   84,   85,   NA },
    {  50,   NA,   52,   53,   54,   55,   56,   57,   58,   59,   60,   61,   NA,   NA,   62,   NA,  111,   NA,   87,   88,   89,  104 },
    {  37,  133,   64,   NA,   NA,   NA,   65,   NA,   NA,   NA,  108,  135,  254,   NA,  105,  113,  116,  114,   90,   NA,   91,   NA }
};

static keyboard ISO_Layout =
{
    {   9,   67,   68,   69,   70,   NA,   71,   72,   73,   74,   NA,   75,   76,   95,   96,  107,   78,  127,   NA,   NA,   NA,   NA },
    {  49,   10,   11,   12,   13,   14,   15,   16,   17,   18,   19,   20,   21,   NA,   22,  118,  110,  112,   77,  106,   63,   82 },
    {  23,   24,   25,   26,   27,   28,   29,   30,   31,   32,   33,   34,   35,   NA,   36,  119,  115,  117,   79,   80,   81,   86 },
    {  66,   38,   39,   40,   41,   42,   43,   44,   45,   46,   47,   48,   NA,   NA,   NA,   NA,   NA,   NA,   83,   84,   85,   NA },
    {  50,   51,   52,   53,   54,   55,   56,   57,   58,   59,   60,   61,   NA,   NA,   62,   NA,  111,   NA,   87,   88,   89,  104 },
    {  37,  133,   64,   NA,   NA,   NA,   65,   NA,   NA,   NA,  108,  135,  254,   NA,  105,  113,  116,  114,   90,   NA,   91,   NA }
};

static scancode_string scancode_strings =
{
//ROW 0
    {   9,  "ESC"  },
    {  67,  "F1"   },
    {  68,  "F2"   },
    {  69,  "F3"   },
    {  70,  "F4"   },
    {  71,  "F5"   },
    {  72,  "F6"   },
    {  73,  "F7"   },
    {  74,  "F8"   },
    {  75,  "F9"   },
    {  76,  "F10"  },
    {  95,  "F11"  },
    {  96,  "F12"  },
    { 107,  "PRTSC"},
    {  78,  "SCRLK"},
    { 127,  "PAUSE"},
//ROW 1
    {  49,  "`"    },
    {  10,  "1"    },
    {  11,  "2"    },
    {  12,  "3"    },
    {  13,  "4"    },
    {  14,  "5"    },
    {  15,  "6"    },
    {  16,  "7"    },
    {  17,  "8"    },
    {  18,  "9"    },
    {  19,  "0"    },
    {  20,  "-"    },
    {  21,  "+"    },
    {  22,  "BKSPC"},
    { 118,  "INSRT"},
    { 110,  "HOME" },
    { 112,  "PGUP" },
    {  77,  "NUMLK"},
    { 106,  "#/"   },
    {  63,  "#*"   },
    {  82,  "#-"   },
//ROW 2
    {  23,  "TAB"  },
    {  24,  "Q"    },
    {  25,  "W"    },
    {  26,  "E"    },
    {  27,  "R"    },
    {  28,  "T"    },
    {  29,  "Y"    },
    {  30,  "U"    },
    {  31,  "I"    },
    {  32,  "O"    },
    {  33,  "P"    },
    {  34,  "["    },
    {  35,  "]"    },
    {  51,  "\\"   },
    { 119,  "DELTE"},
    { 115,  "END"  },
    { 117,  "PGDN" },
    {  79,  "#7"   },
    {  80,  "#8"   },
    {  81,  "#9"   },
    {  86,  "#+"   },
//ROW 3
    {  66,  "CAPLK"},
    {  38,  "A"    },
    {  39,  "S"    },
    {  40,  "D"    },
    {  41,  "F"    },
    {  42,  "G"    },
    {  43,  "H"    },
    {  44,  "J"    },
    {  45,  "K"    },
    {  46,  "L"    },
    {  47,  ";"    },
    {  48,  "'"    },
    {  36,  "ENTER"},
    {  83,  "#4"   },
    {  84,  "#5"   },
    {  85,  "#6"   },
//ROW 4
    {  50,  "LSHFT"},
    {  52,  "Z"    },
    {  53,  "X"    },
    {  54,  "C"    },
    {  55,  "V"    },
    {  56,  "B"    },
    {  57,  "N"    },
    {  58,  "M"    },
    {  59,  ","    },
    {  60,  "."    },
    {  61,  "/"    },
    {  62,  "RSHFT"},
    { 111,  "UP"   },
    {  87,  "#1"   },
    {  88,  "#2"   },
    {  89,  "#3"   },
    { 104,  "#ENTR"},
//ROW 5
    {  37,  "LCTRL"},
    { 133,  "LWIN" },
    {  64,  "LALT" },
    {  65,  "SPACE"},
    { 108,  "RALT" },
    { 135,  "RWIN" },
    { 254,  "FNC"  },
    { 105,  "RCTRL"},
    { 113,  "LEFT" },
    { 116,  "DOWN" },
    { 114,  "RIGHT"},
    {  90,  "#0"   },
    {  91,  "#."   },
};
#endif // OUTPUT_MAPS_H
