#include "mkmap_window.h"
#include "ui_mkmap_window.h"
#include <string>
#include <map>

mkmap_window::mkmap_window(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::mkmap_window)
{
    ui->setupUi(this);

    QAction* action_send_packets = new QAction("Send Packets", ui->menu_Map);
    connect(action_send_packets, SIGNAL(triggered()), this, SLOT(on_send_packets()));
    //ui->menu_Map->addAction(action_send_packets);
}

mkmap_window::~mkmap_window()
{
    if(connected())
    {
        cmmk_set_control_mode( &cmmk_dev, CMMK_FIRMWARE );
        cmmk_detach(&cmmk_dev);
    }

    delete ui;
}

void mkmap_window::keyPressEvent(QKeyEvent * ev)
{
    QString keytext = QString("You pressed key: %1 which prints as %2 with the scancode %3").arg(ev->key(),3,10).arg(ev->text()).arg(ev->nativeScanCode(),3,10);
    ui->statusbar->showMessage(keytext);
}

bool mkmap_window::connected()
{
    int cmmk_pid;

    return connected(&cmmk_pid);
}

bool mkmap_window::connected(int *cmmk_pid)
{
    int profile;
    if(cmmk_connected)
    {
        return true;
    }

    QString item;
    bool ok = (cmmk_find_device(cmmk_pid) == 0);

    if(!ok)
    {
        ok = fallback_detection(&item);
        /*-------------------------------------------------*\
        | If a selection was made then attempt to connect   |
        \*-------------------------------------------------*/
        QString qs_pid  = item.split(":").at(1);
        *cmmk_pid        = qs_pid.toInt(&ok);
    }

    if(cmmk_attach(&cmmk_dev, *cmmk_pid, -1))
    {
        QString error_text = QString("Unable to connected to: %1").arg(item.split(":").at(0));
        QErrorMessage* error_msg = new QErrorMessage(this);
        error_msg->showMessage(error_text);
        ui->statusbar->showMessage(error_text);
        cmmk_connected = false;
    }
    else
    {
        cmmk_set_control_mode( &cmmk_dev, CMMK_MANUAL );
        cmmk_connected = true;
    }

    return cmmk_connected;
}

void mkmap_window::paintKeyboard()
{
    QWidget *wgt_keyboard = ui->widget_keyboard;
    QVBoxLayout *kb_layout = new QVBoxLayout();
    wgt_keyboard->setLayout(kb_layout);

    for(keyboard::iterator kb_it = kb.begin(); kb_it != kb.end(); kb_it++)
    {
        //add a horizontal layout
        QHBoxLayout *hbl_row = new QHBoxLayout();

        for(kb_row::iterator kbr_it = kb_it->begin(); kbr_it != kb_it->end(); kbr_it++)
        {
            if(*kbr_it == NA)
            {
                //Add Spacer
                QWidget* new_space = new QWidget();
                new_space->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
                new_space->setMinimumSize( MIN_WIDGET_SIZE, MIN_WIDGET_SIZE);
                new_space->setMaximumSize( MAX_WIDGET_SIZE, MAX_WIDGET_SIZE);
                hbl_row->addWidget(new_space);
            }
            else
            {
                //Add a button
                std::string key = std::string(scancode_strings.find(*kbr_it)->second);
                QPushButton* new_key = new QPushButton();
                new_key->setFont(QFont("Noto Sans", 8, 0, false));
                new_key->setText(key.c_str());
                new_key->setObjectName(QString("%1").arg(*kbr_it)); //Set the scan code as the name for findChild()
                new_key->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
                new_key->setMinimumSize( MIN_WIDGET_SIZE, MIN_WIDGET_SIZE);
                new_key->setMaximumSize( MAX_WIDGET_SIZE, MAX_WIDGET_SIZE);
                hbl_row->addWidget(new_key);
            }
        }

        QWidget *widget_row = new QWidget();
        widget_row->resize(wgt_keyboard->width(), 270);
        widget_row->setLayout(hbl_row);
        kb_layout->addWidget(widget_row);
    }
}

void mkmap_window::checkKeyboardLayout()
{
    for(std::size_t i = 0; i < MKMAP_LED_ID_END; i++)
    {
        //Fill the unknown LEDs
        kb_map.leds_unknown.push_back(i);
    }

    for(std::size_t kb_row = 0; kb_row < kb.size(); kb_row++)
    {
        for(std::size_t kb_column = 0; kb_column != kb.at(kb_row).size(); kb_column++)
        {
            uint16_t key = kb.at(kb_row).at(kb_column);

            if(key != NA)
            {
                //lookup the row and column in the layout to find the key string.
                scancode_string::iterator sc_str = scancode_strings.find(key);
                QString keyname = QString("%1").arg(sc_str->first);

                //Check if we already know it in the libcmmk map
                uint16_t led_number = MKMAP_LED_ID_NA;
                for(std::size_t i = 0; i < sizeof(cmmk_dev.rowmap); i++)
                {
                    if(cmmk_dev.rowmap[i] == kb_row && cmmk_dev.colmap[i] == kb_column)
                    {
                        led_number = i;
                        break;
                    }
                }

                if(led_number != MKMAP_LED_ID_NA)
                {
                    kb_map.keys_known.push_back(key);
                    for(v_u16::iterator led_it = kb_map.leds_unknown.begin(); led_it != kb_map.leds_unknown.end(); led_it++)
                    {
                        if(*led_it == led_number)
                        {
                            kb_map.leds_unknown.erase(led_it);
                            break;
                        }
                    }

                    QPushButton *key_button = this->findChild<QPushButton *>(keyname);
                    if(key_button)
                    {
                        key_button->setStatusTip(QString("%1").arg(led_number));
                    }
                }
                else
                {
                    kb_map.keys_unknown.push_back(key);
                }
            }
            else
            {
                //Key not used un this layout
            }
        }
    }
}

void mkmap_window::on_action_New_Map_triggered()
{
    int cmmk_pid;
    QString item;
    bool ok = connected(&cmmk_pid);

    if(ok)
    {
        dirty = true;

        /*-------------------------------------------------*\
        | Grab the HID strings for use in the output        |
        \*-------------------------------------------------*/
        const int sz_temp = 255;
        wchar_t tmp_name[sz_temp];
        hid_get_manufacturer_string(cmmk_dev.dev, tmp_name, sz_temp);
        std::wstring w_temp = std::wstring(tmp_name);
        hid_vendor = std::string(w_temp.begin(), w_temp.end());

        hid_get_product_string(cmmk_dev.dev, tmp_name, sz_temp);
        w_temp = std::wstring(tmp_name);
        hid_product = std::string(w_temp.begin(), w_temp.end());

        hid_get_serial_number_string(cmmk_dev.dev, tmp_name, sz_temp);
        w_temp = std::wstring(tmp_name);
        hid_serial = std::string(w_temp.begin(), w_temp.end());

        /*-------------------------------------------------*\
        | Add some UI feedback                              |
        \*-------------------------------------------------*/
        char *kb_name = const_cast<char *>(cmmk_product_to_str(cmmk_dev.product));
        ui->lbl_kb_name->setText(kb_name);

        QString status_text = QString("Connected to: %1").arg(kb_name);
        ui->statusbar->showMessage(status_text);

        kb = (cmmk_get_device_layout(&cmmk_dev) == CMMK_LAYOUT_TYPE_ANSI) ? ANSI_Layout : ISO_Layout;

        paintKeyboard();
        checkKeyboardLayout();

        for(v_u16::iterator led_it = kb_map.leds_unknown.begin(); led_it != kb_map.leds_unknown.end(); )
        {
            status_text = QString("Mapped keys: %1 and Unmapped keys: %2").arg(kb_map.keys_known.size(),3,10).arg(kb_map.keys_unknown.size(),3,10);
            ui->statusbar->showMessage(status_text);

            uint16_t kc = getKeycode(*led_it);

            if(kc != MKMAP_LED_ID_NA)
            {
                QString led_string = QString("%1").arg(*led_it);

                led_it = kb_map.leds_unknown.erase(led_it); //Remove the led_number from the unknown list

                QPushButton *key_button = this->findChild<QPushButton *>(QString("%1").arg(kc));
                kb_map.keys_known.push_back(kc);
                findErase <uint16_t, v_u16> (kc, &kb_map.keys_unknown);

                setStatusOrRemap(led_string, key_button);
            }
            else
            {
                led_it++;
            }
        }

        verifyKeys();
    }
}

uint16_t mkmap_window::getKeycode(uint16_t led_id)
{
    if(cmmk_set_single_key_by_id( &cmmk_dev, led_id, &green) != CMMK_OK)
    {
        QString status_text = QString("No key for led#  %1").arg(led_id, 3, 10);
        ui->statusbar->showMessage(status_text);
        return NA;
    }

    uint16_t scancode = 0;
    key_capture_dialog* dlg_key_capture = new key_capture_dialog(this, &scancode, led_id);
    int ok = dlg_key_capture->exec();

    if(ok != QDialog::Rejected)
    {
        QString status_text = QString("You pressed scancode %1 which is the %2 key").arg(scancode,3,10).arg(std::string(scancode_strings.find(scancode)->second).c_str());
        ui->statusbar->showMessage(status_text);
    }
    else
    {
        scancode = 0;
    }

    cmmk_set_single_key_by_id( &cmmk_dev, led_id, &black);
    return (scancode == 0) ? NA : scancode;
}

void mkmap_window::setStatusOrRemap(QString led_string, QPushButton *button)
{
    if(button || button->statusTip() != led_string)
    {
        if(button->statusTip() == "")
        {
            button->setStatusTip(led_string);
        }
        else
        {
            QMessageBox msg_remap;
            msg_remap.setText("This key is already mapped!");
            msg_remap.setInformativeText(QString("Do you want to remap %1 to led# %2?").arg(button->text()).arg(led_string));
            msg_remap.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            msg_remap.setDefaultButton(QMessageBox::No);
            int remap = msg_remap.exec();

            if(remap == QMessageBox::Yes)
            {
                button->setStatusTip(led_string);
            }
        }
    }
}

template <class S, class V>
void mkmap_window::findErase(S value, V *eraseFrom)
{
    for(class V::iterator it = eraseFrom->begin(); it != eraseFrom->end(); it++)
    {
        if(*it == value)
        {
            eraseFrom->erase(it);
            break;
        }
    }
}

void mkmap_window::verifyKeys()
{
    for(keyboard::iterator kb_it = kb.begin(); kb_it != kb.end(); kb_it++)
    {
        QMessageBox msg_verify;

        for(kb_row::iterator kbr_it = kb_it->begin(); kbr_it != kb_it->end(); kbr_it++)
        {
            if(*kbr_it != NA)
            {
                //Add a button
                std::string key_string = std::string(scancode_strings.find(*kbr_it)->second);
                QPushButton* new_key = msg_verify.addButton(key_string.c_str(), QMessageBox::ActionRole);
                new_key->setFont(QFont("Noto Sans", 8, 0, false));
                new_key->setObjectName(QString("verify_%1").arg(*kbr_it)); //Set the scan code as the name for findChild()
                new_key->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
                new_key->setMinimumSize( MIN_WIDGET_SIZE, MIN_WIDGET_SIZE);
                new_key->setMaximumSize( MAX_WIDGET_SIZE, MAX_WIDGET_SIZE);

                QPushButton *key_button = this->findChild<QPushButton *>(QString("%1").arg(*kbr_it));

                cmmk_set_single_key_by_id( &cmmk_dev, key_button->statusTip().toInt(), &cyan);
            }
        }

        msg_verify.setText("Lighting keys in the %1 row of LEDs!");
        msg_verify.setInformativeText("Please click a button to change search for the LED or click yes to continue.");
        msg_verify.setStandardButtons(QMessageBox::Yes);
        msg_verify.setDefaultButton(QMessageBox::Yes);

        int verify = 0;
        while(true)
        {
            verify = msg_verify.exec();
            if(verify == QMessageBox::Yes)
            {
                break;
            }
            //Change a LED
            QPushButton* change_key = qobject_cast<QPushButton *>(msg_verify.children().at(2)->children().at(verify+1));

            for(kb_row::iterator kbr_it = kb_it->begin(); kbr_it != kb_it->end(); kbr_it++)
            {
                QPushButton *key_button = this->findChild<QPushButton *>(QString("%1").arg(*kbr_it));
                if(*kbr_it != NA)
                {
                    cmmk_set_single_key_by_id( &cmmk_dev, key_button->statusTip().toInt(), &black);
                }
            }

            uint16_t led_number     = recurseKeys(0, kb_map.leds_unknown.size(), change_key->text());
            QPushButton *key_button = this->findChild<QPushButton *>(change_key->objectName().split("_").at(1));
            uint16_t kc             = key_button->objectName().toUInt();

            if(led_number != MKMAP_LED_ID_NA)
            {
                QString led_string = QString("%1").arg(led_number);

                findErase <uint16_t, v_u16> (led_number, &kb_map.leds_unknown);
                kb_map.keys_known.push_back(kc);
                findErase <uint16_t, v_u16> (kc, &kb_map.keys_unknown);

                setStatusOrRemap(led_string, key_button);
            }
            else
            {
                //Handle not found
            }
        }
    }
}

uint16_t mkmap_window::recurseKeys(uint16_t from, uint16_t to, QString keyname)
{
    int found = 0;
    rgb colour1 = green;
    rgb colour2 = magenta;
    QString str_colour1 = QString("green");
    QString str_colour2 = QString("magenta");
    QString str_button  = QString("LED is %1");

    if(from >= to)
    {
        return from;
    }
    else
    {
        uint16_t mid = (from + to) / 2;

        for(uint16_t i = from; i < mid; i++)
        {
            cmmk_set_single_key_by_id( &cmmk_dev, kb_map.leds_unknown.at(i), &colour1);
        }
        for(uint16_t i = mid; i < to; ++i)
        {
            cmmk_set_single_key_by_id( &cmmk_dev, kb_map.leds_unknown.at(i), &colour2);
        }

        QMessageBox msg_recurse;
        msg_recurse.setText(QString("Lighting keys %1 thru %2 %3 and %4 thru %5 %6").arg(from,3,10).arg(((mid <= from) ? (from) : (mid-1))).arg(str_colour1).arg(mid,3,10).arg(to,3,10).arg(str_colour2));
        msg_recurse.setInformativeText(QString("Is the %1 lit up %2 or %3?").arg(keyname).arg(str_colour1).arg(str_colour2));
        QPushButton* led_is_colour1 = msg_recurse.addButton(str_button.arg(str_colour1), QMessageBox::ActionRole);
        QPushButton* led_is_colour2 = msg_recurse.addButton(str_button.arg(str_colour2), QMessageBox::ActionRole);
        //msg_verify.setStandardButtons(QMessageBox::Yes | QMessageBox::No);

        found = msg_recurse.exec();

        if(found == 1)
        {
            for(uint16_t i = from; i < mid; i++)
            {
                cmmk_set_single_key_by_id( &cmmk_dev, kb_map.leds_unknown.at(i), &black);
            }
            return recurseKeys(mid, to, keyname);
        }
        else
        {
            for(uint16_t i = mid; i < to; ++i)
            {
                cmmk_set_single_key_by_id( &cmmk_dev, kb_map.leds_unknown.at(i), &black);
            }
            return recurseKeys(from, mid - 1, keyname);
        }
    }
}

void mkmap_window::on_action_Save_Map_triggered()
{
#ifdef _WIN32
    char *profile;
    size_t length;
    errno_t err = _dupenv_s( &profile, &length, "USERPROFILE" );

    std::string path = std::string(profile);
#else
    std::string path = "~/Documents/";
#endif

    if(dirty)
    {
        QString save_path = QFileDialog::getSaveFileName(this, "Save Map", path.c_str(), "Text Files (*.txt)");

        if(save_path != "")
        {
            std::ofstream out_file(save_path.toStdString(), std::ofstream::out);

            std::string header = "Keyboard:  ";
            header.append(ui->lbl_kb_name->text().append("\n").toStdString());
            header.append("KB Layout: ").append(cmmk_layout_to_str(cmmk_dev.layout)).append("\n\n");
            header.append("HID Vendor String:  ").append(hid_vendor).append("\n");
            header.append("HID Product String: ").append(hid_product).append("\n");
            header.append("HID Serial String:  ").append(hid_serial).append("\n");

            out_file << header << std::endl << libcmmk_format();
            /*---------------------------------------------------------*\
            | Close the file when done                                  |
            \*---------------------------------------------------------*/
            out_file.close();
        }
    }
    else
    {
        ui->statusbar->showMessage("Nothing to save!");
    }
}

std::string mkmap_window::openrgb_format()
{
    std::string out_text;

    for(keyboard::iterator kb_it = kb.begin(); kb_it != kb.end(); kb_it++)
    {
        QStringList *key_strings = new QStringList("/*  ");
        QStringList *led_indexes = new QStringList();

        for(kb_row::iterator kbr_it = kb_it->begin(); kbr_it != kb_it->end(); kbr_it++)
        {
            if(*kbr_it == NA)
            {
                led_indexes->append(QString("-1"));
            }
            else
            {
                QPushButton *key_button = this->findChild<QPushButton *>(QString("%1").arg(*kbr_it));
                led_indexes->append(key_button->statusTip());
            }
        }

        out_text.append(key_strings->join("").toStdString().append("*/\n{ "));
        out_text.append(led_indexes->join(",").toStdString().append("}\n\n"));
    }

    return out_text;
}

std::string mkmap_window::libcmmk_format()
{
    std::string out_text;

    for(keyboard::iterator kb_it = kb.begin(); kb_it != kb.end(); kb_it++)
    {
        QStringList *key_strings = new QStringList("/*   ");
        QStringList *led_indexes = new QStringList();

        for(kb_row::iterator kbr_it = kb_it->begin(); kbr_it != kb_it->end(); kbr_it++)
        {
            if(*kbr_it == NA)
            {
                key_strings->append(QString("  XXX"));
                led_indexes->append(QString("   -1"));
            }
            else
            {
                QPushButton *key_button = this->findChild<QPushButton *>(QString("%1").arg(*kbr_it));
                key_strings->append(QString("%1").arg(key_button->text(), 5, ' '));
                led_indexes->append(QString("%1").arg(key_button->statusTip(), 5, ' '));
            }
        }

        out_text.append(key_strings->join(" ").toStdString().append("     */\n{    "));
        out_text.append(led_indexes->join(",").toStdString().append("},\n\n"));
    }

    return out_text;
}

bool mkmap_window::fallback_detection(QString *item)
{
    /*-------------------------------------------------*\
    | Initialize HID interface for detection            |
    \*-------------------------------------------------*/
    bool result     = false;
    int hid_status  = hid_init();

    QStringList devices;

    hid_device_info *hid_devices = hid_enumerate(COOLERMASTER_VID, 0);

    while(hid_devices)
    {
        std::wstring w_vendor = hid_devices->manufacturer_string;
        std::wstring w_product = hid_devices->product_string;
        //std::wstring w_serial = hid_devices->serial_number;

        std::cout << std::string(w_vendor.begin(), w_vendor.end()) << std::string(w_product.begin(), w_product.end()) << ": PID - " << hid_devices->product_id << " Interface - " << hid_devices->interface_number << std::endl;
        //if(hid_devices->usage_page == COOLERMASTER_USAGE_PAGE && hid_devices->usage == COOLERMASTER_USAGE)
        if(hid_devices->interface_number == 1 && hid_devices->vendor_id == 0x2516)
        {
            std::wstring wName = hid_devices->product_string;
            devices << std::string(wName.begin(), wName.end()).append(" - PID:").append(std::to_string(hid_devices->product_id)).c_str();
        }

        hid_devices = hid_devices->next;
    }
    hid_free_enumeration(hid_devices);

    //QStringList mk_devices = devices.filter("key", Qt::CaseInsensitive);

    if(devices.count() >= 1)
    {
        *item = QInputDialog::getItem(this, "Masterkeys Keyboards","Map which keyboard:", devices, 0, false, &result);
    }
    else
    {
        QString error_text = QString("No Masterkeys keyboards found!<br/>");
        QErrorMessage* error_msg = new QErrorMessage(this);
        error_msg->showMessage(error_text.append(devices.join("<br/>")));
        ui->statusbar->showMessage(error_text);
    }

    return result;
}

void mkmap_window::on_send_packets()
{
    int remap;
    int cmmk_pid;
    const uint8_t buffer_size   = 65;
    bool ok = connected(&cmmk_pid);

    do
    {
        QString text = QInputDialog::getText(this, "Send Packets", "Please enter a list of values to send to the device", QLineEdit::Normal, "", &ok);
        if (ok && !text.isEmpty())
        {
            QStringList msg = text.split(" ");
            uint8_t buffer[buffer_size];
            memset(buffer, 0, sizeof(buffer));

            for(std::size_t i = 0; i < msg.length(); i++)
            {
                uint8_t value = msg[i].toUInt(&ok, 10);
                if(ok)
                {
                    buffer[i] = value;
                }
            }

            uint8_t read_buffer[buffer_size - 1];

            uint8_t w = hid_write(cmmk_dev.dev, buffer, buffer_size);
            uint8_t r = hid_read_timeout(cmmk_dev.dev, read_buffer, buffer_size, 250);

            QString out_string = "Write Buffer:<br/>";
            for(std::size_t i = 0; i < sizeof(buffer); i++)
            {
                out_string.append(QString("%1  ").arg(buffer[i],2,16));
            }
            out_string.append("<br/><br/>Read Buffer:<br/>");
            for(std::size_t i = 0; i < sizeof(buffer); i++)
            {
                out_string.append(QString("%1  ").arg(read_buffer[i],2,16));
            }

            QMessageBox   msg_remap;
            msg_remap.setText("Keyboard Output");
            msg_remap.setInformativeText(out_string);
            msg_remap.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            msg_remap.setDefaultButton(QMessageBox::No);
            remap = msg_remap.exec();
        }
    } while(remap == QMessageBox::Yes);
}
