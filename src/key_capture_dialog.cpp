#include "key_capture_dialog.h"
#include "ui_key_capture_dialog.h"

key_capture_dialog::key_capture_dialog(QWidget *parent, uint16_t *sc, uint16_t led) :
    QDialog(parent),
    ui(new Ui::key_capture_dialog)
{
    ui->setupUi(this);
    ui->bbx_ok_cancel->button(QDialogButtonBox::Ok)->setEnabled(false);
    ui->bbx_ok_cancel->button(QDialogButtonBox::Ok)->setVisible(false);
    ui->bbx_ok_cancel->button(QDialogButtonBox::Cancel)->setVisible(false);
    //ui->bbx_ok_cancel->button(QDialogButtonBox::Cancel)->setEnabled(false);

    scancode = sc;
    this->setWindowTitle(QString("Changing led# %1 to green").arg(led, 3, 10));

    /*-------------------------------------------------*\
    | Add keys and their scancodes to the combobox      |
    \*-------------------------------------------------*/
    for(scancode_string::iterator ss_it = scancode_strings.begin(); ss_it != scancode_strings.end(); ss_it++)
    {
        ui->cbx_special_keys->addItem(ss_it->second.c_str(), QVariant(ss_it->first));
    }
    ui->cbx_special_keys->setCurrentIndex(-1);
    ui->cbx_special_keys->setVisible(false);
}

key_capture_dialog::~key_capture_dialog()
{
    delete ui;
}

void key_capture_dialog::keyPressEvent(QKeyEvent * ev)
{
    *scancode = ev->nativeScanCode();

    /*-------------------------------------------------*\
    | If the scancode is found the return immediately   |
    \*-------------------------------------------------*/
    if(scancode_strings.count(*scancode))
    {
        QDialog::done(QDialog::Accepted);
    }
}

void key_capture_dialog::on_cbx_special_keys_currentIndexChanged(int index)
{
    if(index != -1)
    {
        bool ok = false;
        ui->bbx_ok_cancel->button(QDialogButtonBox::Ok)->setEnabled(true);
        *scancode = ui->cbx_special_keys->itemData(index).toUInt(&ok);
        if(!ok)
        {
            scancode = 0;
        }
    }
    else
    {
        ui->bbx_ok_cancel->button(QDialogButtonBox::Ok)->setEnabled(false);
    }
}
