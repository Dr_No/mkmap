#ifndef OUTPUT_MAPS_H
#define OUTPUT_MAPS_H

#define NA  0xFFFF

typedef std::map<uint16_t, std::string>         scancode_string;
typedef uint16_t                                kb_key;
typedef std::vector<kb_key>                     kb_row;
typedef std::vector<kb_row>                     keyboard;

/*-------------------------------------------------*\
| ANSI_Layout and ISO_Layout is a map of the        |
|   keycodes to an English string for output        |
\*-------------------------------------------------*/

static keyboard SIXTY_Layout =
{
    { true, true, true, true, true,false, true, true, true, true,false, true, true, true, true, true, true, true,false,false,false,false },
    { true, true, true, true, true, true, true, true, true, true, true, true, true,false, true, true, true, true,false,false,false,false },
    { true, true, true, true, true, true, true, true, true, true, true, true, true,false, true, true, true, true,false,false,false,false },
    { true, true, true, true, true, true, true, true, true, true, true, true,false,false, true,false,false,false,false,false,false,false },
    { true, true, true, true, true, true, true, true, true, true, true, true,false,false, true,false, true,false,false,false,false,false },
    { true, true, true,false,false,false, true,false,false,false, true, true, true,false, true, true, true, true,false,false,false,false }
};

static keyboard TKL_Layout =
{
    {   1,    1,    1,    1,    1,    0,    1,    1,    1,    1,    0,    1,    1,    1,    1,    1,    1,    1,    0,    0,    0,    0 },
    {   1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    0,    1,    1,    1,    1,    0,    0,    0,    0 },
    {   1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    0,    1,    1,    1,    1,    0,    0,    0,    0 },
    {   1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    0,    0,    1,    0,    0,    0,    0,    0,    0,    0 },
    {   1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,    0,    0,    1,    0,    1,    0,    0,    0,    0,    0 },
    {   1,    1,    1,    0,    0,    0,    1,    0,    0,    0,    1,    1,    1,    0,    1,    1,    1,    1,    0,    0,    0,    0 }
};

static keyboard ANSI_Layout =
{
    {   1,   59,   60,   61,   62,   NA,   63,   64,   65,   66,   NA,   67,   68,   87,   88,   89,   70,   69,   NA,   NA,   NA,   NA },
    {  41,    2,    3,    4,    5,    6,    7,    8,    9,   10,   11,   12,   13,   NA,   14,  338,  327,  329,  325,  309,   55,   74 },
    {  15,   16,   17,   18,   19,   20,   44,   22,   23,   24,   25,   26,   27,   NA,   43,  339,  335,  337,   71,   72,   73,   78 },
    {  58,   30,   31,   32,   33,   34,   35,   36,   37,   38,   39,   40,   NA,   NA,   28,   NA,   NA,   NA,   75,   76,   77,   NA },
    {  42,   NA,   21,   45,   46,   47,   48,   49,   50,   51,   52,   53,   NA,   NA,   54,   NA,  111,   NA,   79,   80,   81,  284 },
    {  29,  347,   56,   NA,   NA,   NA,   57,   NA,   NA,   NA,  312,  348,  254,   NA,  285,  113,  116,  114,   82,   NA,   83,   NA }
};

static keyboard ISO_Layout =
{
    {   1,   59,   60,   61,   62,   NA,   63,   64,   65,   66,   NA,   67,   68,   87,   88,   89,   70,   69,   NA,   NA,   NA,   NA },
    {  41,    2,    3,    4,    5,    6,    7,    8,    9,   10,   11,   12,   13,   NA,   14,  338,  327,  329,  325,  309,   55,   74 },
    {  15,   16,   17,   18,   19,   20,   44,   22,   23,   24,   25,   26,   27,   NA,   28,  339,  335,  337,   71,   72,   73,   78 },
    {  58,   30,   31,   32,   33,   34,   35,   36,   37,   38,   39,   40,   NA,   NA,   NA,   NA,   NA,   NA,   75,   76,   77,   NA },
    {  42,   86,   21,   45,   46,   47,   48,   49,   50,   51,   52,   53,   NA,   NA,   54,   NA,  111,   NA,   79,   80,   81,  284 },
    {  29,  347,   56,   NA,   NA,   NA,   57,   NA,   NA,   NA,  312,  348,  254,   NA,  285,  113,  116,  114,   82,   NA,   83,   NA }
};

static scancode_string scancode_strings =
{
//ROW 0
    {   1,  "ESC"  },
    {  59,  "F1"   },
    {  60,  "F2"   },
    {  61,  "F3"   },
    {  62,  "F4"   },
    {  63,  "F5"   },
    {  64,  "F6"   },
    {  65,  "F7"   },
    {  66,  "F8"   },
    {  67,  "F9"   },
    {  68,  "F10"  },
    {  87,  "F11"  },
    {  88,  "F12"  },
    {  89,  "PRTSC"},
    {  70,  "SCRLK"},
    {  69,  "PAUSE"},
//ROW 1
    {  41,  "`"    },
    {   2,  "1"    },
    {   3,  "2"    },
    {   4,  "3"    },
    {   5,  "4"    },
    {   6,  "5"    },
    {   7,  "6"    },
    {   8,  "7"    },
    {   9,  "8"    },
    {  10,  "9"    },
    {  11,  "0"    },
    {  12,  "-"    },
    {  13,  "+"    },
    {  14,  "BKSPC"},
    { 338,  "INSRT"},
    { 327,  "HOME" },
    { 329,  "PGUP" },
    { 325,  "NUMLK"},
    { 309,  "#/"   },
    {  55,  "#*"   },
    {  74,  "#-"   },
//ROW 2
    {  15,  "TAB"  },
    {  16,  "Q"    },
    {  17,  "W"    },
    {  18,  "E"    },
    {  19,  "R"    },
    {  20,  "T"    },
    {  44,  "Y"    },
    {  22,  "U"    },
    {  23,  "I"    },
    {  24,  "O"    },
    {  25,  "P"    },
    {  26,  "["    },
    {  27,  "]"    },
    {  43,  "\\"   },
    { 339,  "DELTE"},
    { 335,  "END"  },
    { 337,  "PGDN" },
    {  71,  "#7"   },
    {  72,  "#8"   },
    {  73,  "#9"   },
    {  78,  "#+"   },
//ROW 3
    {  58,  "CAPLK"},
    {  30,  "A"    },
    {  31,  "S"    },
    {  32,  "D"    },
    {  33,  "F"    },
    {  34,  "G"    },
    {  35,  "H"    },
    {  36,  "J"    },
    {  37,  "K"    },
    {  38,  "L"    },
    {  39,  ";"    },
    {  40,  "'"    },
    {  28,  "ENTER"},
    {  75,  "#4"   },
    {  76,  "#5"   },
    {  77,  "#6"   },
//ROW 4
    {  42,  "LSHFT"},
    {  86,  "<"    },
    {  21,  "Z"    },
    {  45,  "X"    },
    {  46,  "C"    },
    {  47,  "V"    },
    {  48,  "B"    },
    {  49,  "N"    },
    {  50,  "M"    },
    {  51,  ","    },
    {  52,  "."    },
    {  53,  "/"    },
    {  54,  "RSHFT"},
    { 111,  "UP"   },
    {  79,  "#1"   },
    {  80,  "#2"   },
    {  81,  "#3"   },
    { 284,  "#ENTR"},
//ROW 5
    {  29,  "LCTRL"},
    { 347,  "LWIN" },
    {  56,  "LALT" },
    {  57,  "SPACE"},
    { 312,  "RALT" },
    { 348,  "RWIN" },
    { 254,  "FNC"  },
    { 105,  "RCTRL"},
    { 113,  "LEFT" },
    { 116,  "DOWN" },
    { 114,  "RIGHT"},
    {  82,  "#0"   },
    {  83,  "#."   },
};
#endif // OUTPUT_MAPS_H
