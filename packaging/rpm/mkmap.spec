%global _name mkmap

Name:           mkmap
Version:        0.1
Release:        1%{?dist}
Summary:        A LED layout mapper for Coolermaster Masterkey keyboards.

License:        GPLv2
URL:            https://gitlab.com/Dr_No/%{_name}
Source0:        mkmap.tar.gz

BuildRequires:  gcc-c++ libusb-devel libstdc++-devel qt5-qtbase-devel hidapi-devel 
Requires:       hicolor-icon-theme

%description
A LED layout mapper for Coolermaster Masterkey keyboards for use with libcmmk and OpenRGB.

%prep
cp %{_sourcedir}/%{_name}/* %{_builddir} -r

%build
cd %{_builddir}
/usr/bin/cmake %{_sourcedir}/%{_name}
%make_build

%install
%make_install INSTALL_ROOT=%{buildroot}

#icon
install -Dpm 644 src/%{_name}_logo.png %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/%{_name}_logo.png

#desktop
install -Dpm 644 %{_sourcedir}/%{_name}/packaging/%{_name}.desktop %{buildroot}%{_datadir}/applications/%{_name}.desktop

%post -n %{name}
if [ -S /run/udev/control ]; then
    udevadm control --reload
    udevadm trigger
fi

%files
%{_bindir}/%{name}
%{_datadir}/icons/hicolor/128x128/apps/%{_name}_logo.png
%{_datadir}/applications/%{_name}.desktop
%{_datadir}/pixmaps/%{_name}_logo.png
%{_includedir}/libcmmk-config.h
%{_includedir}/libcmmk/libcmmk.h
%{_libdir}/../lib/libcmmk.a
%{_libdir}/../lib/libcmmk.so
/lib/udev/rules.d/60-%{name}.rules
%license LICENSE

%changelog
* Fri Sep 10 2021 Chris M <galdarian@gmail.com> - 0.1
- Initial import
